// https://docs.oracle.com/javase/7/docs/api/java/util/Hashtable.html

import java.util.ArrayList;
import java.util.Hashtable;

public class Puzzle {

   /** Solve the word puzzle.
    * @param args three words (addend1 addend2 sum)
    */
	public static String A="ABCDEFGHIJAB";
	public static String B="ABCDEFGHIJA";
	public static String C="ACEHJBDFGIAC";
	
	static boolean[] numbers = new boolean[10];
	public static int pikkus;
	
	public static ArrayList<String> lettersarray;
	
	public static Hashtable<String, Integer> lettersmap;
	
	public static int lahendcount;
	
	private static void kontroll () {
		if (lettersmap.get(A.substring(0,1))==0) {
			return;
		}
		if (lettersmap.get(B.substring(0,1))==0) {
			return;
		}
		if (decode(A)+decode(B)==decode(C)) {
			lahendcount++;
			if(lahendcount==1) {
				System.out.println("Esimene lahendus on: " + lettersmap);
			}						
		}
	}
	
	private static int decode (String s) {
		String X = "";
		for (int i = 0; i < s.length(); i++) {
			X += Long.toString(lettersmap.get(s.substring(i,i+1)));
		}
		return (int) Long.parseLong(X);		
	}
	
	public static void main (String[] args) {
		A= args[0];
		B= args[1];
		C= args[2];
		
		lahendcount=0;
		
		lettersarray = new ArrayList<String>();
		
		lettersmap = new Hashtable<String, Integer>();
		
		for (int i = 0; i < A.length(); i++){
			lettersmap.put(A.substring(i,i+1),0);        
		} 
		for (int i = 0; i < B.length(); i++){
			lettersmap.put(B.substring(i,i+1),0);        
		}
		for (int i = 0; i < C.length(); i++){
			lettersmap.put(C.substring(i,i+1),0);        
		}
		//System.out.println(lettersmap);
		
		for (String key : lettersmap.keySet()) {
			lettersarray.add(key);
		}
		//System.out.println(lettersarray.get(2));
		 
		pikkus=lettersarray.size();
		if (pikkus>10 ) {
			throw new RuntimeException("Liiga palju t�hti!");
		}
				
		while (true) {
			//System.out.println(lettersmap);
			boolean validsetup = false;
			for (int i=0; i<pikkus;i++) {
				validsetup = false;
				for (int j=lettersmap.get(lettersarray.get(i)); j<10;j++) {
					if (! numbers[j]) {
						lettersmap.put(lettersarray.get(i), j);
						numbers[j]=true;
						//System.out.println(lettersarray.get(i));
						//System.out.println(lettersmap.get(lettersarray.get(i)));
						validsetup = true;
						break;
					}					
				}
				if (!validsetup) {
					break;
				}
			}
			// System.out.println(lettersmap);
			if (validsetup) {
				kontroll();
			}
			
			for (int i = (pikkus-1);i>=0 ;i--) {
				int pos = lettersmap.get(lettersarray.get(i));
				if (pos < 9) {
					lettersmap.put(lettersarray.get(i), pos+1);
					break;
				} else {
					lettersmap.put(lettersarray.get(i), 0);
					if (i==0) {
						System.out.println("Kokku leitud lahendusi on " + lahendcount);
						return;
					}
				}												
			}
			for (int i = 0; i < 10; i++) {
				numbers[i] = false;
			}
			//System.out.println(A);
			//System.out.println(decode (A));	
		}
	}
}

